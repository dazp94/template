import React from "react";

export default class DropDown extends React.Component {
  state = { openDrop: false };

  render() {
    return (
      <div
        style={{
          width: 100,
          height: 0,
          position: "relative"
        }}
      >
        <div onClick={() => this.setState({ openDrop: !this.state.openDrop })}>
          {this.props.children}
        </div>
        {this.state.openDrop && this.renderDrops()}
      </div>
    );
  }

  renderDrops = () => {
    let divs = [];
    let entries = this.props.array;
    {
      for (let i = 0; i < entries.length; i++) {
        divs.push(
          <div style={{ marginTop: i * 20 }}>{this.renderDrop(entries[i])}</div>
        );
      }

      return (
        <div
          style={{
            position: "absolute"
          }}
        >
          {divs}
        </div>
      );
    }
  };
  renderDrop = entry => {
    return (
      <div
        style={{
          zIndex: 10,
          width: "100%",
          height: "100%",
          position: "absolute",
          left: 0
        }}
      >
        {entry}
      </div>
    );
  };
}
